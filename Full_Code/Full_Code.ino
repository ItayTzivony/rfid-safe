#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <FastLED.h>
#include <Vector.h>
#include "pitches.h"

uint8_t getID();
void readTag();
short lookForTag(String tagID);
void playMusic(int melody[], int noteDurations[], int numNotes);
void printTags(); //debug
void showLEDs(byte r, byte g, byte b);

#define BUZ_PIN A0
#define LED_PIN A1
#define RST_PIN 9
#define SS_PIN 10

#define NUM_LEDS 8
#define MAX_TAGS 10
#define OPEN_ANGLE 180
#define CLOSE_ANGLE 0

byte readCard[4];
String tagID = "";
short tagIndex;
boolean successRead;
String storage_array[MAX_TAGS];

// Create instances
MFRC522 mfrc522(SS_PIN, RST_PIN);
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
Servo servo;
Vector<String> tags;
CRGB leds[NUM_LEDS];

// ----- Musics -----

// #1 access granted

#define NUM_NOTES_1 8

// notes in the melody:
int melody1[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations1[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

// #2 access denied

#define NUM_NOTES_2 5

int melody2[] = {
  NOTE_F4, NOTE_E4, NOTE_F4, NOTE_E4, NOTE_F4
};

int noteDurations2[] = {
  16, 16, 16, 16, 16
};

// #3 add tag

#define NUM_NOTES_3 4

int melody3[] = {
  NOTE_F3, NOTE_C4, NOTE_F4, NOTE_A4
};

int noteDurations3[] = {
  8, 8, 8, 8
};

// #4 remove tag

#define NUM_NOTES_4 4

int melody4[] = {
  NOTE_A4, NOTE_F4, NOTE_C4, NOTE_F3
};

int noteDurations4[] = {
  8, 8, 8, 8
};

// #5 set master tag

#define NUM_NOTES_5 15

int melody5[] = {
  NOTE_C4, 0, 0, NOTE_D4, 0, 0, NOTE_C4, 0, 0, 0, NOTE_C4, 0, NOTE_D4, 0, NOTE_E4
};

int noteDurations5[] = {
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16
};

// #6 add/remove master tag failure

#define NUM_NOTES_6 17

int melody6[] = {
  NOTE_C4, 0, 0, NOTE_D4, 0, 0, NOTE_C4, 0, 0, 0, NOTE_F4, 0, NOTE_E4, 0, NOTE_D4, 0, NOTE_B3 
};

int noteDurations6[] = {
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 8
};

// -----------------

void setup() {
  Serial.begin(9600); //Debug
  // Initiating
  SPI.begin(); //SPI bus
  mfrc522.PCD_Init(); //MFRC522
  lcd.begin(16, 2); //LCD screen
  servo.attach(8); //Servo mottor
  tags.setStorage(storage_array); //Tags vector
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  servo.write(CLOSE_ANGLE);
  
  lcd.setCursor(0, 0);
  lcd.print("Scan master tag");
  readTag();
  showLEDs(255, 255, 255);
  playMusic(melody5, noteDurations5, NUM_NOTES_5);
  showLEDs(0, 0, 0);

  tags.push_back(tagID); //Sets the master tag to first element in vector
  lcd.clear();
}

void loop() {
  lcd.setCursor(0, 0);
  lcd.print("Scan a tag");
  readTag();
  lcd.clear();

  tagIndex = lookForTag(tagID);
  if (tagIndex == -1) //unverified tag
  {
    lcd.setCursor(0, 0);
    lcd.print("Unverified tag");
    lcd.setCursor(0, 1);
    lcd.print("Access denied");
    showLEDs(255, 0, 0);
    playMusic(melody2, noteDurations2, NUM_NOTES_2);
    showLEDs(0, 0, 0);
    delay(1000);
  }
  else if (tagIndex == 0) //master tag
  {
    lcd.setCursor(0, 0);
    lcd.print("Program mode");
    lcd.setCursor(0, 1);
    lcd.print("Add/Remove a tag");
    readTag();
    lcd.clear();

    tagIndex = lookForTag(tagID);
    if (tagIndex == -1) //add new tag
    {
      tags.push_back(tagID);
      lcd.setCursor(0, 0);
      lcd.print("New tag added");
      showLEDs(0, 0, 255);
      playMusic(melody3, noteDurations3, NUM_NOTES_3);
      showLEDs(0, 0, 0);
    }
    else if (tagIndex == 0) //can't remove master tag
    {
      lcd.setCursor(0, 0);
      lcd.print("Can't remove");
      lcd.setCursor(0, 1);
      lcd.print("master tag");
      showLEDs(255, 0, 0);
      playMusic(melody6, noteDurations6, NUM_NOTES_6);
      showLEDs(0, 0, 0);
    }
    else //remove existing tag
    {
      tags.remove(tagIndex);
      lcd.setCursor(0, 0);
      lcd.print("Existing tag");
      lcd.setCursor(0, 1);
      lcd.print("removed");
      showLEDs(0, 0, 255);
      playMusic(melody4, noteDurations4, NUM_NOTES_4);
      showLEDs(0, 0, 0);
    }
    delay(1000);
  }
  else //verified tag
  {
    lcd.setCursor(0, 0);
    lcd.print("Verified tag");
    lcd.setCursor(0, 1);
    lcd.print("Access granted");

    servo.write(OPEN_ANGLE);
    showLEDs(0, 255, 0);
    playMusic(melody1, noteDurations1, NUM_NOTES_1);
    showLEDs(0, 0, 0);
    servo.write(CLOSE_ANGLE);
  }
  lcd.clear();
}

uint8_t getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  tagID = "";
  for ( uint8_t i = 0; i < 4; i++) {  // The MIFARE PICCs that we use have 4 byte UID
    readCard[i] = mfrc522.uid.uidByte[i];
    tagID.concat(String(mfrc522.uid.uidByte[i], HEX)); // Adds the 4 bytes in a single String variable
  }
  tagID.toUpperCase();
  mfrc522.PICC_HaltA(); // Stop reading
  return 1;
}

void readTag() {
  successRead = false;
  while (!successRead)
  {
    successRead = getID();
  }
}

short lookForTag(String tagID) {
  for (short i = 0; i < tags.size(); i++)
  {
    if (tagID == tags[i])
    {
      return i;
    }
  }

  return -1;
}

void playMusic(int melody[], int noteDurations[], int numNotes) {
  for (int thisNote = 0; thisNote < numNotes; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(BUZ_PIN, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(BUZ_PIN);
  }
}

void printTags() {
  Serial.print("size: ");
  Serial.println(tags.size());
  for (short i = 0; i < tags.size(); i++)
  {
    Serial.println(tags[i]);
  }
}

void showLEDs(byte r, byte g, byte b)
{
  for(int i = 0; i < 8; i++)
  {
    leds[i] = CRGB(r, g, b);
  }
  FastLED.show();
}
